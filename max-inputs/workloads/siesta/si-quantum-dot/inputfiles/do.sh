#!/bin/bash
#SBATCH --job-name=test-qdot
#SBATCH --account=Max3_devel_1
#SBATCH --partition=m100_usr_prod
#SBATCH --output=mpi_%j.out 
#SBATCH --error=mpi_%j.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --gres=gpu:4
#SBATCH --time=00:59:00

# (NOTE Yambo-style bindings)
#
ml purge
ml gcc_env
ml siesta-elsi-profile/1.5-elsi-gpu-no-elpa
#
date
which siesta
echo "-------------------"
#
export OMP_NUM_THREADS=1
## export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#
rm -rf /tmp/nvidia
ln -s $TMPDIR /tmp/nvidia
mpirun --map-by socket:PE=8 --rank-by core --report-bindings \
       -np ${SLURM_NTASKS} bash wrap.sh  \
       siesta si-quantum-dot.fdf
rm -rf /tmp/nvidia

